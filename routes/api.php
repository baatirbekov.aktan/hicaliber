<?php

use App\Http\Controllers\HouseController;
use Illuminate\Support\Facades\Route;

Route::resource('houses', HouseController::class)->only(['index']);
