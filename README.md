# Project Setup and Development Guide

This guide will help you set up your development environment using Docker and provide you with the necessary commands to manage your application effectively.

## Prerequisites

Before you begin, ensure you have Docker installed on your machine. If you haven't installed Docker yet, please visit [Docker's official website](https://www.docker.com/get-started) for installation instructions.

## Getting Started

To get your environment up and running with minimal effort, we've encapsulated the setup process into a Makefile. Here's how you can initialize your project:

```bash
make init
