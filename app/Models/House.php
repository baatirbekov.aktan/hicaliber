<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'price', 'bedrooms', 'bathrooms', 'storeys', 'garages'];

    /**
     * @param $query
     * @param  array  $filters
     * @return void
     */
    public function scopeFilter($query, array $filters): void
    {
        if (!empty($filters['name'])) {
            $query->where('name', 'LIKE', "%{$filters['name']}%");
        }

        if (!empty($filters['bedrooms'])) {
            $query->where('bedrooms', '=', $filters['bedrooms']);
        }

        if (!empty($filters['bathrooms'])) {
            $query->where('bathrooms', '=', $filters['bathrooms']);
        }

        if (!empty($filters['storeys'])) {
            $query->where('storeys', '=', $filters['storeys']);
        }

        if (!empty($filters['garages'])) {
            $query->where('garages', '=', $filters['garages']);
        }

        if (!empty($filters['price'])) {
            $query->whereBetween('price', $filters['price']);
        }
    }
}
