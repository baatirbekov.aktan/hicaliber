import { createStore } from 'vuex'
import houses from "@/store/modules/houses"

export default createStore({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
      houses
  }
})
