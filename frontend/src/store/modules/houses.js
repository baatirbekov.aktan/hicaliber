import houses from "@/api/houses";

const state = {
    houses: null,
    housesIsLoading: false,
    housesMeta: null,
};

const getters = {
    getHouses: (state) => {
        return state.houses
    },
    getHousesIsLoading: (state) => {
        return state.housesIsLoading
    },
    getHousesMeta: (state) => {
        return state.housesMeta
    },
}

const mutations = {
    setHousesStart: (state) => {
        state.housesIsLoading = true;
        state.houses = null;
    },
    setHousesSuccess: (state, payload) => {
        state.housesIsLoading = false;
        state.houses = payload.data;
        state.housesMeta = payload.meta;
    },
    setHousesFailure: (state) => {
        state.housesIsLoading = false;
    },
};

const actions = {
    getHouses(context, queryParams) {
        return new Promise((resolve) => {
            context.commit("setHousesStart");
            houses
                .getHouses(queryParams)
                .then((response) => {
                    context.commit("setHousesSuccess", response.data);
                    resolve(response.data);
                })
                .catch((result) => {
                    context.commit("setHousesFailure");
                });
        });
    },
};

export default {
    state,
    getters,
    mutations,
    actions
};
