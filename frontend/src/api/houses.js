import axios from "@/plugins/axios";

const getHouses = (queryParams) => {
    return axios.get('houses', {params: queryParams})
}

export default {
    getHouses
}
