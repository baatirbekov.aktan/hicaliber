<?php

namespace Database\Seeders;

use App\Models\House;
use Illuminate\Database\Seeder;

class HouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $contents = file_get_contents(storage_path('csv/property-data.csv'));

        $csvRows = explode("\n", $contents);

        $headers = str_getcsv(array_shift($csvRows));

        $adIdIndex = array_search('Name', $headers);
        $adGroupIdIndex = array_search('Price', $headers);
        $campaignIdIndex = array_search('Bedrooms', $headers);
        $costIndex = array_search('Bathrooms', $headers);
        $currencyIndex = array_search('Storeys', $headers);
        $adGroupIndex = array_search('Garages', $headers);

        $data = [];
        $now = now();

        for ($i = 0; $i < count($csvRows); $i++) {
            $row = str_getcsv($csvRows[$i]);

            $data[] = [
                'name' => $row[$adIdIndex],
                'price' => $row[$adGroupIdIndex],
                'bedrooms' => $row[$campaignIdIndex],
                'bathrooms' => $row[$costIndex],
                'storeys' => $row[$currencyIndex],
                'garages' => $row[$adGroupIndex],
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        House::query()->insert($data);
        House::factory()->count(100)->create();
    }
}
